import { formatDistanceToNowStrict, endOfToday, isBefore, endOfTomorrow, isMonday, isTuesday, isWednesday, isThursday, isFriday, isSaturday, isSunday, addDays } from 'date-fns';
import { fr } from 'date-fns/locale';

export function useIsNumeric(number) {
    const integer_pattern = /-?\d+$/;
    const float_pattern = /^\d+(\.|,)\d+$/;
    return number.match(integer_pattern) != null || number.match(float_pattern) != null;
}

export function useIsGreaterThanZero(number) {
    if(!isNaN(number)) {
        const _float = parseFloat(number)
        return _float > 0;
    }
    else {
        return false;
    }
}

export function useComputeRelativeTime(start_date) {
    if(start_date != "JJ/MM") {
        let relative_time = "";
        // Overriding w.r.t. LAS requirements
        if(isBefore(start_date, endOfToday())) {
            relative_time = "Aujourd'hui";
        }
        else if(isBefore(start_date, endOfTomorrow())) {
            relative_time = "Demain";
        }
        else if(isBefore(start_date, addDays(new Date(), 6))) {
            if(isMonday(start_date)) {
                relative_time = "Lundi";
            }
            else if(isTuesday(start_date)) {
                relative_time = "Mardi";
            }
            else if(isWednesday(start_date)) {
                relative_time = "Mercredi";
            }
            else if(isThursday(start_date)) {
                relative_time = "Jeudi";
            }
            else if(isFriday(start_date)) {
                relative_time = "Vendredi";
            }
            else if(isSaturday(start_date)) {
                relative_time = "Samedi";
            }
            else if(isSunday(start_date)) {
                relative_time = "Dimanche";
            }
        }
        else {
            if (start_date){
                            relative_time = formatDistanceToNowStrict(start_date, { locale: fr });
            }else{
                relative_time="";
            }
        }
        return relative_time;
    }
    else {
        return "Bientôt";
    }
}