/*
export function reOrderByDates(obj) {
  return obj.dates.reduce((acc, cur) => {
    acc.push({
      event_name: obj.name,
      event_thumbnail: obj.thumbnail,
      event_description: obj.description,
      event_url: obj.post_event_url,
      event_organizer: obj.main_organizer[0].name,
      event_main_category: obj.main_category[0].name,
      event_other_organizers: obj.other_organizers.length > 0 ? obj.other_organizers : [],
      event_other_categories: obj.other_categories.length > 0 ? obj.other_categories : [],
      id: cur.id,
      name: cur.name,
      description: cur.description,
      location: cur.location.name,
      start_date: cur.start_date,
      end_date: cur.end_date,
      start_hour: cur.start_hour,
      end_hour: cur.end_hour,
      prices: obj.prices
    });
    return acc;
  }, [])
}
*/

/**
 * 
 * @param {*} obj un objet fournit par l'API date/[uid]
 * @returns un tableau destiné aux components DisplaySmallLeaf
 */
export function reOrderByDatesUID(obj) {

  return obj.associated_dates.reduce((acc, cur) => {
    acc.push({
      event_name: obj.event.name,
        event_thumbnail: obj.event.thumbnail,
      event_description: obj.event.description,     // <<<<<<<<<<<<<<< voir a retrouver l'index de evt courrant
      event_url:obj.post_event_url,                                             // <<<<<<<<<<<<<<< manque
      event_organizer: obj.event.main_organizer[0].name,
      event_main_category: obj.main_category.name,
      event_other_organizers: obj.event.other_organizers.length > 0 ? obj.event.other_organizers : [],
      event_other_categories: obj.other_categories.length > 0 ? obj.other_categories : [],                               // <<<<<<<<<<<<<<< manque
      id: cur.uid,
      name: cur.name,
      description: cur.description,
      location: cur.location.name,
      start_date: cur.start_date,
      end_date: cur.end_date,
      start_hour: cur.start_hour,
      end_hour: cur.end_hour,
      prices: obj.event.prices
    });
    return acc;
  }, [])
}

