

/**
 * test si une adresse mail est valide
 * 
 * source du regex: https://emailregex.com/
 * 
 * @param {*} value valeur de l'adresse email à tester
 * @returns true si l'adresse email' est valide
 */
export function isAdresseMailValide (value){
    var adresseMailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    return adresseMailRegex.test (value);
}