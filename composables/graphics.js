// Module gathering common methods for graphics

export function useSetDangerBackground() {
    return "bg-red-300";
}

export function getClassWarning () {
    return " ring-2 ring-red-500 placeholder:text-red-500 placeholder:font-bold";
}

export function useSetWarning(div_id) {
    const div = document.getElementById(div_id);
    if(div != null) {
        div.setAttribute("class", div.getAttribute("class") + " ring-2 ring-red-500");
    }
}

function setInputWarning(input, class_warning) {
    if(input.value == "") {
        input.setAttribute("class", input.getAttribute("class") + class_warning);
    }
}
function handleInputWarning(input, class_warning) {
    if(input.value != "") {
        input.setAttribute("class", input.getAttribute("class").replaceAll(class_warning, ""));
    }
    else {
        input.setAttribute("class", input.getAttribute("class") + class_warning);
    }
}

export function useRequiredInputEventListener(input_id, button_id) {
    const button = document.getElementById(button_id);
    if(button != null) {
        const input = document.getElementById(input_id);
        const class_warning = getClassWarning ();
        if(input != null) {
            button.addEventListener('click', setInputWarning(input, class_warning));
            input.addEventListener('input', handleInputWarning(input, class_warning));
        }
    }
}

export function useRemoveRequiredInputEventListener(input_id, button_id) {
    const button = document.getElementById(button_id);
    const input = document.getElementById(input_id);
    if(button != null) {
        button.removeEventListener("click", setInputWarning);
    }
    if(input != null) {
        input.removeEventListener("input", handleInputWarning);
    }
}


/* Add an event listener for a click outside a div
 * /!\ This function shall be called after a component is mounted
 * Parameters:
 *     div_id : the identifier of the div the click shall be "outside of"
 *     f_action : the function to execute when the click happens
 */
export function useClickOutsideEventListener(div_id, f_action) {
    document.addEventListener('click', (e) => {
        const click_div = document.getElementById(div_id);
        if(click_div != null && !click_div.contains(e.target)) {
            f_action();
        }
    });
}