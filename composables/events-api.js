import { computed, watch } from 'vue';
import { active_filter } from '~/directives/filter';

export function useEventsApi() {
    active_filter.computeFilterFromUrl(useRoute().query);
    const apiUrl = useRuntimeConfig().public.apiUrl;
   
    const apiQuery = computed(() => {
        return active_filter.eventsQueryParams;
    })

    const fullApiUrl = computed(() => {
        const query = apiQuery.value;
        return Object.keys(query).length > 0 ? `${apiUrl}/filter` : `${apiUrl}/dates`;
    })

    watch(
        apiQuery,
        () => {
            useRouter().push({query: apiQuery.value});
        }
    );

    return useFetch(
        fullApiUrl,
        {query: apiQuery}
    );
}

