/**
 * test la validité d'un numéro de téléphone
 * 
 * source du regex: https://regexpattern.com/phone-number/#fr
 * voir getFormatPhoneNumber () pour les formats acceptés.
 * 
 * @param {*} value valeur du numéro de téléphone à tester
 * @returns true si le numéro de téléphone est valide
 */
export function isPhoneValide (value) {
    var telephoneRegex = /^(?:(?:\+|00)33[\s.-]{0,3}(?:\(0\)[\s.-]{0,3})?|0)[1-9](?:(?:[\s.-]?\d{2}){4}|\d{2}(?:[\s.-]?\d{3}){2})$/;

    return telephoneRegex.test (value);
}
/**
 * format accepté par isPhoneValide (value).
 * 
 * @returns les formats de numéros de téléphones acceptés
 */
export function getFormatPhoneNumber () {
    return "0123456789\n01 23 45 67 89\n01.23.45.67.89\n0123 45.67.89\n0033 123-456-789\n+33-1.23.45.67.89\n+33 – 123 456 789\n+33(0) 123 456 789\n+33 (0)123 45 67 89\n+33 (0)1 2345-6789\n+33(0) – 123456789";
}