
/**
 * test la validité d'une adresse Web
 * 
 * source du regex: https://stackoverflow.com/questions/3809401/what-is-a-good-regular-expression-to-match-a-url
 * 
 * @param {*} value valeur à tester
 * @returns true si la valeur est valide
 */
export function isAdressWebValide (value) {
    var adressWebRegex = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/;
    
    return adressWebRegex.test (value);
}

/**
 * test la validité d'une adresse Facebook
 * 
 * source du regex: https://regex101.com/r/pP6qO3/1
 * 
 * @param {*} value valeur à tester
 * @returns true si la valeur est valide
 */
export function isAdressFaceBookValide (value) {    
    var adressFacebookRegex = /^(http\:\/\/|https\:\/\/)?(?:www\.)?facebook\.com\/(?:(?:\w\.)*#!\/)?(?:pages\/)?(?:[\w\-\.]*\/)*([\w\-\.]*)/;

    return adressFacebookRegex.test (value);
}

/**
 * test la validité d'une adresse Instagram
 * 
 * source du regex: https://regex101.com/r/Ci3DA9/1
 * 
 * @param {*} value valeur à tester
 * @returns true si la valeur est valide
 */
export function isAdressInstagramValide (value) {
    var adressInstagramRegex = /(?:(?:http|https):\/\/)?(?:www.)?(?:instagram.com|instagr.am|instagr.com)\/(\w+)/;

    return adressInstagramRegex.test (value);
}