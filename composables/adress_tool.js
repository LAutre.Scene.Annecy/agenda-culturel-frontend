/**
 * test la validité d'un code postal
 * 
 * source du regex: https://www.gekkode.com/developpement/expression-reguliere-pour-valider-un-code-postal/
 *                  présente quelques code de pays mitoyens qui peuvent être ajouté.
 * 
 * on peut eventuellement évoluer sur un code postal internationnal voir: https://www.data.gouv.fr/fr/reuses/validate-zip-codes-with-regular-expressions/
 * 
 * @param {*} value valeur du code postal à tester
 * @returns true si le code postal est valide
 */
export function isCodePostalValide (value) {
    var codePostalRegex = /^(?:0[1-9]|[1-8]\d|9[0-8])\d{3}$/;

    return codePostalRegex.test (value);
}