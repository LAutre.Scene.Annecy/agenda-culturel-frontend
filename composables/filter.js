export function filterByName(list, value) {
    if(value.trim() != "") {
        const new_list = [];
        list.forEach(element => {
            if(element.name.toLowerCase().includes(value.toLowerCase())) {
                new_list.push(element);
            }
        });
        return new_list;
    }
    else {
        return list;
    }
}
