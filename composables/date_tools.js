/**
 * 
 * @param {*} d date dont on recherche le libellé du jour de la semaine
 * @returns le libellé du jour de la semaine de la date
 */
export function getLabelDayOfWeek (d) {
    const jours = ["Dimanche","Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi"];

    return jours[d.getDay ()]
}


/**
 * 
 * @param {*} aDate un objet fournit par l'API date/[uid]
 * @returns l'index de l'objet dans son tableau interne de "associated_dates"
 */
export function findDateIndexInAssociatedDates (aDate) {
    var index=0;
    var resultat=-1;

    while (resultat<0 && index<=aDate.associated_dates.length){
        if (aDate.associated_dates[index].uid===aDate.uid){
            resultat=index;
        }
        index++;
    }

    return resultat;
}

/**
 * 
 * @param {*} entries_list vérifie la validité de la liste d'horraire d'ouverture 
 * @returns true si les horraires d'ouvertures sont valides.
 */
export function isReceptionScheduleDayValide (entries_list) {
    var datesValide=true;

    var dateReference = new Date ();
    dateReference.setUTCHours(0);
    dateReference.setUTCMinutes(0);
    
    var i=0;

    while (i<entries_list.length && datesValide){
            var elt=entries_list[i];

        if (Array.isArray(elt)
            && typeof elt[0] === 'string'
            && typeof elt[1] === 'string'
            ){       
            const d1=new Date ();
            const d2=new Date ();
        
            d1.setUTCHours(elt[0].split (":")[0]);
            d1.setUTCMinutes(elt[0].split (":")[1]);
        
            d2.setUTCHours(elt[1].split (":")[0]);
            d2.setUTCMinutes(elt[1].split (":")[1]);
        
            if (isNaN(d1)){
                datesValide=false;
            }else if (isNaN(d2)){
                datesValide=false;
            }else if (d1<dateReference){
                datesValide=false;
            }else if (d2<=d1){
                datesValide=false;
            }

            dateReference=d2;
        }else{
            datesValide=false;
        }
            i++;
        }
    
        return datesValide;
}