/** @type {import('tailwindcss').Config} */
export default {
  content: [
    "./components/**/*.{js,vue,ts}",
    "./layouts/**/*.vue",
    "./pages/**/*.vue",
    "./plugins/**/*.{js,ts}",
    "./app.vue",
    "./error.vue",
  ],
  theme: {
    extend: {
      fontFamily: {
        las: ["Cream-Text", "Verdana", "sans-serif"],
        "las-title": ["Cream-Title", "Verdana", "sans-serif"],
        sans: ["Cream-Text", "Verdana", "sans-serif"],
      },
      colors: {
        "las-green": "#4db276",
        "las-orange": "#e58252",
        "las-pink": "#e88ab7",
        "las-grey": "#E9EAEC",
        "shadow-las-dark": "#6b7280",
      },
      borderRadius: {
        "las-cat": "0% 100% 39% 61% / 0% 0% 100% 100%",
        "las-date": "7% 10% 24% 10% / 10% 10% 43% 86%",
        "las-event": "33% 65% 0% 0% / 15% 36% 10% 10%",
        "filters-block-top": "25px 10px 0 0",
        "filters-block-bottom": "0 0 10px 10px",
      },
      backgroundImage: {
        "las-image-2": "url('/images/illus.svg')",
        "las-card-event-date": "url('/images/bg_date_event.svg')",
        "las-card-event-catg": "url('/images/bg_lieu_event.svg')",
        "las-card-event-catg-dark": "url('/images/bg_lieu_event_dark.svg')",
      },
      backgroundSize: {
        auto70: "auto 70%",
      },
      boxShadow: {
        las: "0px 1px 4px rgba(0, 0, 0, 0.25)",
        "las-gallery-tile": "5px 5px 20px 5px rgba(0,0,0,0.25)",
      },
      fontSize: {
        "sm+": ["15px", "20px"],
        size2240: ["22px", "40px"],
        size2520: ["25px", "20px"],
        "base+": ["16px", "30px"],
      },
      spacing: {
        space15: "15px",
        space20: "20px",
        space50: "50px",
      },
    },
  },
  plugins: [],
};
