// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },

  // Manually added
  css: ['~/assets/css/main.css'],

  postcss: {
    plugins: {
      tailwindcss: {},
      autoprefixer: {},
    },
  },

  router: {
    options: {
      linkActiveClass: "active",
      linkExactActiveClass: "exact-active"
    }
  },

  modules: ["@nuxt/image", [
    'nuxt-mail',
    {
      message: {
        to: process.env.MAIL_TO,
      },
      smtp: {
        host: process.env.MAIL_HOST,
        port: process.env.MAIL_PORT || 587,
        auth: {
          user: process.env.MAIL_USER,
          pass: process.env.MAIL_PASS,
        },
      },
    },
  ], '@nuxtjs/leaflet'],

  build: {
    transpile: ['@vuepic/vue-datepicker']
  },

  plugins: [
    '~/plugins/mitt.client.js'
  ],

  devServer: {},
  compatibilityDate: '2024-08-08',

  runtimeConfig: {
    public: {
      mailFrom: process.env.MAIL_FROM,
      apiUrl: process.env.NUXT_API_BASE,
    },
  },
  routeRules: {
    '/': { redirect: '/gallery' },
  },
  leaflet: {
    markerCluster: true
  }
})
