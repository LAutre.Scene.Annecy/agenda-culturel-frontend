import { IdentifiableElement } from '~/adapters/manager.js';

export class Category extends IdentifiableElement {
    constructor(name) {
        super();
        this.name = name;
        this.is_main = false;
        this.parent = null; // null or name of the parent
    }

    isMain() {
        return this.is_main;
    }

    setIsMain(value) {
        this.is_main = value;
    }

    isEqual(other_category) {
        if (typeof(this) != typeof(other_category)) {
            return false;
        }
        else {
            // Two categories are considered equal if they have the same name
            return (this.name == other_category.name);
        }
    }   

    computeJson() {
        const json = {
            is_main_category: this.is_main,
            name: this.name
        };
        if(this.parent != null) {
            return Object.assign({}, json, {parent_uid: this.parent.db_uid});
        }
        else {
            return json;
        }
    }

    getParentName() {
        return (this.parent != null) ? this.parent : null;
    }
}

export class ExistingCategory extends Category {
    constructor(name, db_uid, parent) {
        super(name);
        this.db_uid = db_uid;
        this.parent = parent;
    }

    computeJson() {
        const json = super.computeJson();
        delete json.name;
        const json_specific = {
            uid: this.db_uid
        }
        return Object.assign({}, json, json_specific);
    }
}