import { IdentifiableElement } from '~/adapters/manager.js';
import { setOptionalField } from '~/adapters/utils.js';

export class Price extends IdentifiableElement {
    constructor() {
        super();
        this.name = null;
        this.ticket_office = null;
    }

    isEqual(other_price) {
        if(typeof(this) != typeof(other_price)) {
            return false;
        }
        else {
            const this_uid = this.uid;
            delete this.uid;
            const json_this = JSON.stringify(this);
            this.uid = this_uid;
            
            const other_price_uid = other_price.uid;
            delete other_price.uid;
            const json_other_price = JSON.stringify(other_price);
            other_price.uid = other_price_uid;

            return json_this == json_other_price;
        }
    }

    displayText() {
        return this.name;
    }

    computeJson() {
        let json = {};

        if(this.ticket_office != null) {
            json = Object.assign({}, json, this.ticket_office.computeJson());
        }
        
        // Optional fields
        setOptionalField(json, "name", this.name);

        return json;
    }
}

export class FixedPrice extends Price {
    constructor() {
        super();
        this.value = null;
    }

    displayText() {
        return this.name + " | " + this.value + "€";
    }

    computeJson() {
        const json_specific = {
            type: "fixed",
            value: this.value
        }
        return Object.assign({}, super.computeJson(), json_specific);
    }
}

export class OpenPrice extends Price {
    constructor() {
        super();
        this.minimum = null;
    }

    displayText() {
        return this.name + " > " + this.minimum + "€";
    }

    computeJson() {
        const json_specific = {
            type: "open",
            value: this.minimum
        }
        return Object.assign({}, super.computeJson(), json_specific);
    }
}

export class TicketOffice {
    constructor() {
        this.is_onsite = false;
    }

    computeJson() {
        return {
            ticket_office: "on site"
        };
    }
}

export class OnlineTicketOffice extends TicketOffice {
    constructor() {
        super();
        this.url = null;
    }
    
    computeJson() {
        return {
            ticket_office: this.url
        };
    }
}