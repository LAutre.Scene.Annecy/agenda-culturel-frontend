import { Manager } from "~/adapters/manager.js";
import { Date } from "~/adapters/date.js";
import { holder_manager } from "~/adapters/holder_manager.js";

import { emitter } from "~/plugins/mitt.client.js";


//import { defineEmits } from "vue";
//import mitt from "mitt";

//export const emitter = mitt();


class DateManager extends Manager {
    constructor() {
        super();
        // At least one date
        this._add(new Date());
        this.active_date = this.getList()[0];
    }

    removeByIndex(index) {
        const active_date_index = this.getActiveDateIndex();
        super.removeByIndex(index);
        if(active_date_index == index) {
            this.setActiveDate(this.list.length - 1);
        }
    }

    reset() {
        super.reset();
        // At least one date
        this._add(new Date());
        this.active_date = this.getList()[0];
    }

    register(date) {
        const index = super.register(date);
        this.setActiveDate(index);
    }




    
    setActiveDate(index) {
        if(index < this.list.length) {
            this.active_date.setIsActive(false);
            this.list[index].setIsActive(true);
            this.active_date = this.list[index];

            emitter.emit("dateManager.activeDateChange", { index: index });

        }
    }







    setLocation(location) {
        if(this.active_date.location != null) {
            this.removeLocation();
        }
        this.active_date.setLocation(location);
        holder_manager.register(location);
    }

    removeLocation() {
        const location_uid = this.active_date.location.uid;
        this.active_date.location = null;
        if(!this._isLocationReferenced(location_uid)) {
            holder_manager.remove(location_uid);
        }
    }

    checkRequiredData(is_multiple_dates) {
        for(let i=0; i<this.list.length; i++) {
            const is_date_ok = this.list[i].checkRequiredData(is_multiple_dates);
            if(!is_date_ok) {
                this.setActiveDate(i);
                return false;
            }
        }
        return true;
    }
    checkLocation() {
        return this.getActiveDate().location != null;
    }
    checkPrices() {
        return this.getActiveDate().price_manager.getList().length > 0;
    }

    getActiveDate() {
        return this.active_date;
    }
    getActiveDateIndex() {
        return this._findIndexByObject(this.active_date);
    }

    getActiveName() {
        return this.active_date.name;
    }
    getActiveStartDate() {
        return this.active_date.start_date;
    }
    getActiveEndDate() {
        return this.active_date.end_date;
    }
    getActiveStartHour() {
        return this.active_date.start_hour;
    }
    getActiveEndHour() {
        return this.active_date.end_hour;
    }
    getActiveLocation() {
        return this.active_date.location;
    }

    computePricesJson() {
        const json_list = [];
        this.getList().forEach((date) => {
            const list_prices = date.price_manager.computeJson(date.start_date, date.end_date);
            list_prices.forEach((json_price) => {
                json_list.push(json_price);
            });
        });
        return json_list;
    }

    computeDatesJson() {
        const json_list = [];
        this.getList().forEach((date) => {
            json_list.push(date.computeJson());
        });
        return json_list;
    }

    // Private methods
    _isLocationReferenced(location_uid) {
        this.getList().forEach((date) => {
            if(date.location != null && date.location.uid == location_uid) {
                return true;
            }
        });
        return false;
    }

}

export const date_manager = reactive(new DateManager());