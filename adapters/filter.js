import { format } from "date-fns";
import { fr } from 'date-fns/locale';


import { IdentifiableElement } from "./manager.js";
import { CategoryManager } from "./category_manager.js";
import { HolderManager } from "./holder_manager.js";

export class EventFilter {
    constructor() {
        this.has_changed = false;
        this.date_filter = new DateFilter();
        this.category_filter = new CategoryManager();
        this.organizer_filter = new HolderManager();
        this.location_filter = new HolderManager();
        this.price_filter = new PriceFilter();
    }

    // Copy the data of this filter to another filter
    feed(filter) {
        this.has_changed = false;
        this.date_filter.fetch(filter.date_filter);
        this.category_filter.fetch(filter.category_filter);
        this.organizer_filter.fetch(filter.organizer_filter);
        this.location_filter.fetch(filter.location_filter);
        this.price_filter.fetch(filter.price_filter);
    }

    hasChanged() {
        return this.has_changed;
    }

    isActive() {
        return (this.date_filter.isSet()
            || !this.category_filter.isEmpty()
            || !this.organizer_filter.isEmpty()
            || !this.location_filter.isEmpty()
            || this.price_filter.isSet());
    }

    reset() {
        this.has_changed = false;
        this.date_filter.reset();
        this.category_filter.reset();
        this.organizer_filter.reset();
        this.location_filter.reset();
        this.price_filter.reset();
    }

    get eventsQueryParams() {
        const categories_params = this.category_filter.computeUrlParameters();
        const organizers_params = this.organizer_filter.computeOrganizersUrlParameters();
        const locations_params = this.location_filter.computeLocationsUrlParameters();
        const dates_params = this.date_filter.computeUrlParameters();
        const prices_params = this.price_filter.computeUrlParameters();

        return {
            ...categories_params,
            ...organizers_params,
            ...locations_params,
            ...dates_params,
            ...prices_params,
        };
    }

    computeFilterFromUrl(query) {
        if (query != null) {
            this.category_filter.computeFilterFromUrl(query.categories);
            this.organizer_filter.computeOrganizersFilterFromUrl(query.organizers);
            this.location_filter.computeLocationsFilterFromUrl(query.locations);
            this.date_filter.computeFilterFromUrl(query.dates);
            this.price_filter.computeFilterFromUrl(query.prices);
        }
    }

    removeDate() {
        this.has_changed = true;
        this.date_filter.reset();
    }
    removeCategory(uid) {
        this.has_changed = true;
        this.category_filter.remove(uid);
    }
    removeOrganizer(uid) {
        this.has_changed = true;
        this.organizer_filter.remove(uid);
    }
    removeLocation(uid) {
        this.has_changed = true;
        this.location_filter.remove(uid);
    }
    removePrice() {
        this.has_changed = true;
        this.price_filter.reset();
    }
}

class DateFilter extends IdentifiableElement {
    constructor() {
        super();
        this.start_date = null;
        this.end_date = null;
    }

    fetch(date_filter) {
        this.start_date = (date_filter.start_date != null) ? date_filter.start_date : this.start_date;
        this.end_date = (date_filter.end_date != null) ? date_filter.end_date : this.end_date;
    }

    isSet() {
        return this.start_date != null && this.end_date != null;
    }

    display() {
        let display = "";
        if (this.isSet()) {
            if (this.start_date != this.end_date) {
                display = `du ${format(this.start_date, "d MMMM", { locale: fr })} au ${format(this.end_date, "d MMMM", { locale: fr })}`;
            }
            else {
                display = `le ${format(this.start_date, "d MMMM", { locale: fr })}`;
            }
        }
        return display;
    }

    reset() {
        this.start_date = null;
        this.end_date = null;
    }

    computeUrlParameters() {
        return this.start_date != null && this.end_date != null ? { dates: format(this.start_date, "yyyy-MM-dd") + "," + format(this.end_date, "yyyy-MM-dd") } : undefined;
    }

    computeFilterFromUrl(filter) {
        if (typeof filter === "string") {
            const dates = filter.split(",");
            if (dates.length == 2) {
                this.start_date = dates[0];
                this.end_date = dates[1];
            }
        }
    }
}

class PriceFilter extends IdentifiableElement {
    constructor() {
        super();
        this.has_changed = false;
        this.is_ranged = false;
        this.range_value = null;
        this.is_open = false;
        this.is_free = false;
    }

    fetch(price_filter) {
        if (price_filter.has_changed) {
            this.is_ranged = price_filter.is_ranged;
            this.range_value = price_filter.range_value;
            this.is_open = price_filter.is_open;
            this.is_free = price_filter.is_free;
        }
    }

    isSet() {
        return this.is_ranged || this.is_free || this.is_open;
    }

    reset() {
        this.has_changed = false;
        this.is_ranged = false;
        this.range_value = null;
        this.is_open = false;
        this.is_free = false;
    }

    display() {
        let display = "";
        if (this.is_ranged) {
            display = `entre ${this.range_value[0]}€ et ${this.range_value[1]}€`;
        }
        else if (this.is_open) {
            display = "prix libre";
        }
        else if (this.is_free) {
            display = "gratuit";
        }
        return display;
    }

    computeUrlParameters() {
        let value;
        if (this.is_ranged && this.range_value != null) {
            value = this.range_value.join(",");
        }
        else if (this.is_open) {
            value = "open";
        }
        else if (this.is_free) {
            value = "free";
        }
        return value ? { prices: value } : undefined;
    }

    computeFilterFromUrl(filter) {
        if (typeof filter === "string") {
            if (filter == "open") {
                this.is_open = true;
            }
            else if (filter == "free") {
                this.is_free = true;
            }
            else if (filter.split(",").length == 2) {
                this.is_ranged = true;
                const prices = filter.split(",");
                this.range_value = [prices[0], prices[1]];
            }
        }
    }
}