import {v4 as uuid} from 'uuid';

export class IdentifiableElement {
    constructor() {
        this.uid = uuid();
    }

    // Shall be overriden by subclasses
    isEqual(other_element) {
        return typeof(this) == typeof(other_element);
    }
}

export class Manager {
    constructor() {
        this.list = [];
    }

    reset() {
        this.list = [];
    }

    getList() {
        return this.list;
    }

    isEmpty() {
        return this.list.length == 0;
    }

    register(element) {
        let index = this._findIndexByObject(element);
        if(index == -1) {
            this._add(element);
            index = this.list.length - 1;
        }
        return index;
    }

    fetch(manager) {
        manager.list.forEach(element => {
            this.register(element);
        })
    }

    removeByObject(element) {
        const index = this._findIndexByObject(element);
        if(index != -1) {
            this.list.splice(index, 1);
        }
    }

    removeByIndex(index) {
        if(index >= 0 && index < this.list.length) {
            this.list.splice(index, 1);
        }
    }

    remove(uid) {
        const index = this._findIndexByUid(uid);
        if(index != -1) {
            this.list.splice(index, 1);
        }
    }

    // Private methods
    _add(element) {
        this.list.push(element);
    }

    _findIndexByObject(element) {
        let index = -1;
        for(let i=0; i<this.list.length; i++) {
            if(element.isEqual(this.list[i])) {
                index = i;
                break;
            }
        }
        return index;
    }

    _findIndexByUid(uid) {
        let index = -1;
        for(let i=0; i<this.list.length; i++) {
            if(uid == this.list[i].uid) {
                index = i;
                break;
            }
        }
        return index;
    }
}