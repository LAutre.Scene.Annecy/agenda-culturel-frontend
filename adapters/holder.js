import { IdentifiableElement } from '~/adapters/manager.js';
import { holder_manager } from '~/adapters/holder_manager.js';
import { setOptionalField } from '~/adapters/utils.js';

export class Holder extends IdentifiableElement {
    constructor() {
        super();
        this.json_id = null;
        this.is_location = false;
        this.is_organizer = false;
        this.name = null;
        this.is_main = false;
    }

    setJsonId(value) {
        this.json_id = value;
    }

    setIsLocation(value) {
        this.is_location = value;
    }

    setIsOrganizer(value) {
        this.is_organizer = value;
    }

    setName(value) {
        this.name = value;
    }

    setIsMain(value) {
        this.is_main = value;
    }

    isOrganizer() {
        return this.is_organizer;
    }

    isLocation() {
        return this.is_location;
    }

    isEqual(other_holder) {
        if (typeof (this) != typeof (other_holder)) {
            return false;
        }
        else {
            const this_uid = this.uid;
            const this_is_main = this.is_main;
            const this_json_id = this.json_id;
            delete this.json_id;
            delete this.uid;
            delete this.is_main;
            const json_this = JSON.stringify(this);
            this.uid = this_uid;
            this.is_main = this_is_main;
            this.json_id = this_json_id;

            const other_holder_uid = other_holder.uid;
            const other_holder_is_main = other_holder.is_main;
            const other_holder_json_id = other_holder.json_id;
            delete other_holder.json_id;
            delete other_holder.uid;
            delete other_holder.is_main;
            const json_other_holder = JSON.stringify(other_holder);
            other_holder.uid = other_holder_uid;
            other_holder.is_main = other_holder_is_main;
            other_holder.json_id = other_holder_json_id;

            return json_this == json_other_holder;
        }
    }

    computeHolderJson() {
        return {
            id: this.json_id
        };
    }

    computeOrganizerJson() {
        if (this.is_organizer) {
            return {
                id: holder_manager.getJsonId(this.uid),
                is_main_organizer: this.is_main
            }
        }
    }

    computeLocationJson() {
        if (this.is_location) {
            return {
                id: holder_manager.getJsonId(this.uid)
            }
        }
    }
}

export class ExistingHolder extends Holder {
    constructor(is_organizer, is_location, name, uid) {
        super();
        this.is_organizer = is_organizer;
        this.is_location = is_location;
        this.name = name;
        this.db_uid = uid;
    }

    computeHolderJson() {
        const specific_json = {
            uid: this.db_uid
        };
        return Object.assign({}, super.computeHolderJson(), specific_json);
    }
}

export class NewHolder extends Holder {
    constructor() {
        super();
        this.is_ephemeral = false;
        this.logo = null;
        this.thumbnail = null;
        this.address = null;
        this.website_url = null;
        this.description = null;
        this.email = null;
        this.phone_number = null;
        this.social_networks_urls = null;
        this.latitude = null;
        this.longitude = null;
        this.reception_hours = null;
    }

    setIsEphemeral(value) {
        this.is_ephemeral = value;
    }

    setLogo(value) {
        if (value.trim() != "") {
            this.logo = value;
        }
    }

    setThumbnail(value) {
        if (value.trim() != "") {
            this.thumbnail = value;
        }
    }

    setAddress(value) {
        if (value.trim() != "") {
            this.address = value;
        }
    }

    setWebsiteUrl(value) {
        if (value.trim() != "") {
            this.website_url = value;
        }
    }

    setDescription(value) {
        if (value.trim() != "") {
            this.description = value;
        }
    }

    setEmail(value) {
        if (value.trim() != "") {
            this.email = value;
        }
    }

    setPhoneNumber(value) {
        if (value.trim() != "") {
            this.phone_number = value;
        }
    }

    setSocialNetworksUrls(value) {
        if (value.trim() != "") {
            this.social_networks_urls = value;
        }
    }

    setLatitude(value) {
        if (value !== null) {
            this.latitude = value;
        }
    }

    setLongitude(value) {
        if (value !== null) {
            this.longitude = value;
        }
    }

    setReceptionHours(value) {
        if (value != null) {
            this.reception_hours = value;
        }
    }

    computeHolderJson() {
        let specific_json = {
            is_organizer: this.is_organizer,
            is_location: this.is_location,
            is_ephemeral: this.is_ephemeral,
            name: this.name
        };

        // Optional fields
        specific_json = setOptionalField(specific_json, "logo", this.logo);
        specific_json = setOptionalField(specific_json, "thumbnail", this.thumbnail);
        specific_json = setOptionalField(specific_json, "address", this.address);
        specific_json = setOptionalField(specific_json, "website_url", this.website_url);
        specific_json = setOptionalField(specific_json, "description", this.description);
        specific_json = setOptionalField(specific_json, "email", this.email);
        specific_json = setOptionalField(specific_json, "phone_number", this.phone_number);
        specific_json = setOptionalField(specific_json, "social_networks_urls", this.social_networks_urls);
        specific_json = setOptionalField(specific_json, "latitude", this.latitude);
        specific_json = setOptionalField(specific_json, "longitude", this.longitude);
        specific_json = setOptionalField(specific_json, "reception_hours", this.reception_hours);

        return Object.assign({}, super.computeHolderJson(), specific_json);
    }
}