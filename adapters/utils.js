export function setOptionalField(target, field_name, field) {
    if(field != null) {
        target[field_name] = field;
    }
    return target;
}

