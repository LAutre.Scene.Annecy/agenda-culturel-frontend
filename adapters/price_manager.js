import { Manager } from "~/adapters/manager.js";

export class PriceManager extends Manager {
    constructor() {
        super();
    }

    checkRequiredData() {
        return this.list.length > 0;
    }

    computeJson(start_date, end_date) {
        const json_date_details = {
            start_date: start_date,
            end_date: end_date
        };
        const json_list = [];
        this.getList().forEach(price => {
            const json_price = price.computeJson();
            json_list.push(Object.assign({}, json_date_details, json_price));
        })
        return json_list;
    }
}