export class Hours {
    constructor(day) {
        this.day = day;
        this.hours = [];
    }

    computeJson() {
        let json_field = "";
        if (this.hours.length == 0) {
            json_field = "closed";
        }
        else {
            let is_first = true;
            for(let i=0; i<this.hours.length; i++) {
                if(is_first) {
                    is_first = false;
                    json_field = this.hours[i][0] + ":" + this.hours[i][1];
                }
                else {
                    json_field += ";" + this.hours[i][0] + ":" + this.hours[i][1];
                }
            }
        }
        return json_field;
    }

    reset() {
        this.hours = [];
    }
}

export class Reception {
    constructor() {
        this.monday = new Hours("Lundi");
        this.tuesday = new Hours("Mardi");
        this.wednesday = new Hours("Mercredi");
        this.thursday = new Hours("Jeudi");
        this.friday = new Hours("Vendredi");
        this.saturday = new Hours("Samedi");
        this.sunday = new Hours("Dimanche");
    }

    computeJson() {
        let json = {
            monday: this.monday.computeJson(),
            tuesday: this.tuesday.computeJson(),
            wednesday: this.wednesday.computeJson(),
            thursday: this.thursday.computeJson(),
            friday: this.friday.computeJson(),
            saturday: this.saturday.computeJson(),
            sunday: this.sunday.computeJson()
        }
        return json;
    }

    reset() {
        this.monday.reset();
        this.tuesday.reset();
        this.wednesday.reset();
        this.thursday.reset();
        this.friday.reset();
        this.saturday.reset();
        this.sunday.reset();
    }
}

export const reception = reactive(new Reception());