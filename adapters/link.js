import { IdentifiableElement } from '~/adapters/manager.js';

export class Link extends IdentifiableElement {
    constructor(image, text) {
        super();
        this.image = image;
        this.text = text;
        this.value = null;
    }

    isEqual(other_link) {
        if(typeof(this) != typeof(other_link)) {
            return false;
        }
        else {
            return JSON.stringify(this) == JSON.stringify(other_link);
        }
    }

    isWebsiteLink() {
        return this.text == "Site";
    }

    toString() {
        return (this.value != null && this.value.trim() != "") ? this.text + ":" + this.value.trim() + ";" : ""; 
    }

    setValue(value) {
        this.value = value;
    }
}