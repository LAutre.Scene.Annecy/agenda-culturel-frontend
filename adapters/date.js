import { format } from 'date-fns';
import { PriceManager } from '~/adapters/price_manager.js';
import { IdentifiableElement } from '~/adapters/manager.js';
import { setOptionalField } from '~/adapters/utils.js';

export class Date extends IdentifiableElement {
    constructor() {
        super();
        this.is_active = true;
        this.start_date = null;
        this.end_date = null;
        this.name = null;
        this.description = null;
        this.is_online = false;
        this.start_hour = null;
        this.end_hour = null;
        this.location = null;
        this.is_accessible_after_start = false;
        this.price_manager = new PriceManager();
    }

    isEqual(other_date) {
        if(typeof(this) != typeof(other_date)) {
            return false;
        }
        else {
            return JSON.stringify(this) == JSON.stringify(other_date);
        }
    }

    display() {
        return (this.start_date) ? format(this.start_date, 'dd/MM') : "";
    }

    checkRequiredData(is_multiple_dates) {
        if(this.start_date == null) {
            return false;
        }
        else if(this.end_date == null) {
            return false;
        }
        else if(is_multiple_dates && this.name == null) {
            return false;
        }
        else if(is_multiple_dates && this.description == null) {
            return false;
        }
        else if(this.start_hour == null) {
            return false;
        }
        else if(this.end_hour == null) {
            return false;
        }
        else if(this.location == null) {
            return false;
        }
        else if(!this.price_manager.checkRequiredData()) {
            return false;
        }
        else {
            return true;
        }
    }

    computeJson() {
        let json = {
            start_date: this.start_date,
            start_hour: this.start_hour,
            end_hour: this.end_hour,
            location: (this.location != null) ? this.location.computeLocationJson() : null,
            is_accessible_after_start: this.is_accessible_after_start
        }

        // Optional fields
        json = setOptionalField(json, "end_date", this.end_date);
        json = setOptionalField(json, "name", this.name);
        json = setOptionalField(json, "description", this.description);
        json = setOptionalField(json, "is_online", this.is_online);

        return json;
    }

    setIsActive(value) {
        this.is_active = value;
    }

    setStartDate(start_date) {
        this.start_date = start_date;
    }

    setEndDate(end_date) {
        this.end_date = end_date;
    }

    setName(name) {
        this.name = (name.trim() != "") ? name : null;
    }

    setDescription(description) {
        this.description = (description.trim() != "") ? description : null;
    }

    setIsOnline(is_online) {
        this.is_online = is_online;
    }

    setStartHour(start_hour) {
        this.start_hour = start_hour;
    }

    setEndHour(end_hour) {
        this.end_hour = end_hour;
    }

    setLocation(location) {
        this.location = location;
    }

    setIsAccessibleAfterStart(is_accessible_after_start) {
        this.is_accessible_after_start = is_accessible_after_start;
    }
}