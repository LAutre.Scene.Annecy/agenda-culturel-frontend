import { Manager } from "~/adapters/manager.js";
import { ExistingHolder } from "./holder";

export class HolderManager extends Manager {

    constructor() {
        super();
    }

    getOrganizersList() {
        return this.list.filter(item => item.is_organizer);
    }

    getLocationsList() {
        return this.list.filter(item => item.is_location);
    }

    register(element) {
        const index = super.register(element)
        if (!this._hasMainOrganizer()) {
            this._setFirstOrganizerMain();
        }
        return index;
    }

    remove(uid) {
        super.remove(uid);
        if (!this._hasMainOrganizer()) {
            this._setFirstOrganizerMain();
        }
    }

    checkRequiredData() {
        return this.getOrganizerNameList().length > 0 && this.getLocationNameList().length > 0;
    }

    checkOrganizers() {
        return this.getOrganizerNameList().length > 0;
    }
    checkLocations() {
        return this.getLocationNameList().length > 0;
    }

    getOrganizerNameList() {
        const name_list = [];
        this.list.forEach(holder => {
            if (holder.is_organizer) {
                name_list.push(holder.name);
            }
        })
        return name_list;
    }

    getLocationNameList() {
        const name_list = [];
        this.list.forEach(holder => {
            if (holder.is_location) {
                name_list.push(holder.name);
            }
        })
        return name_list;
    }

    setMainOrganizer(uid) {
        const index = this._findIndexByUid(uid);
        if ((index != -1) && (this.getList()[index].is_organizer)) {
            this._setMainOrganizer(index);
        }
    }

    computeHoldersJson() {
        const json_list = [];
        this.getList().forEach((holder, index) => {
            holder.setJsonId(index + 1);
            json_list.push(holder.computeHolderJson());
        })
        return json_list;
    }

    computeOrganizersJson() {
        const json_list = [];
        this.getList().forEach((holder) => {
            if (holder.is_organizer) {
                json_list.push(holder.computeOrganizerJson());
            }
        });
        return json_list;
    }

    computeOrganizersUrlParameters() {
        return this.list.length > 0 ? { organizers: this.list.map(i => i.name) } : undefined;
    }

    computeLocationsUrlParameters() {
        return this.list.length > 0 ? { locations: this.list.map(i => i.name) } : undefined;
    }

    computeOrganizersFilterFromUrl(filter) {
        if (!filter) {
            return;
        }
        this.list = (Array.isArray(filter) ? filter : [filter]).map(element => new ExistingHolder(
            true,
            false,
            element,
            null
        ));
    }

    computeLocationsFilterFromUrl(filter) {
        if (!filter) {
            return;
        }
        this.list = (Array.isArray(filter) ? filter : [filter]).map(element => new ExistingHolder(
            false,
            true,
            element,
            null
        ));
    }

    getJsonId(uid) {
        let json_id = null;
        for (let i = 0; i < this.getList().length; i++) {
            if (uid == this.getList()[i].uid) {
                json_id = this.getList()[i].json_id;
                break;
            }
        }
        return json_id;
    }

    // Private methods
    _resetMainOrganizer() {
        this.list.forEach(holder => {
            holder.setIsMain(false);
        });
    }
    _setMainOrganizer(index) {
        this._resetMainOrganizer();
        for (let i = 0; i < this.getList().length; i++) {
            if ((i == index) && (this.list[i].is_organizer)) {
                this.list[i].setIsMain(true);
                break;
            }
        }
    }

    _setFirstOrganizerMain() {
        this._resetMainOrganizer();
        for (let i = 0; i < this.getList().length; i++) {
            if (this.list[i].is_organizer) {
                this.list[i].setIsMain(true);
                break;
            }
        }
    }

    _hasMainOrganizer() {
        for (let i = 0; i < this.getList().length; i++) {
            if (this.getList()[i].is_organizer && this.getList()[i].is_main) {
                return true;
            }
        }
        return false;
    }
}

export const holder_manager = reactive(new HolderManager());