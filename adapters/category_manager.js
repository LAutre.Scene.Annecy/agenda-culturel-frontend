import { Manager } from '~/adapters/manager.js';
import { Category } from '~/adapters/category.js';

export class CategoryManager extends Manager {

    constructor() {
        super();
    }

    register(element) {
        const index = super.register(element);
        if (!this._hasMainCategory()) {
            this._setMainCategory(0);
        }
        return index;
    }

    remove(uid) {
        super.remove(uid);
        if (!this._hasMainCategory()) {
            this._setMainCategory(0);
        }
    }

    checkRequiredData() {
        return this.list.length > 0;
    }

    getNameList() {
        const name_list = [];
        this.list.forEach(category => {
            name_list.push(category.name);
        })
        return name_list;
    }

    setMainCategory(uid) {
        const index = this._findIndexByUid(uid);
        if (index != -1) {
            this._setMainCategory(index);
        }
    }

    getMainCategory() {
        let main_category = null;
        for (let i = 0; i < this.list.length; i++) {
            if (this.list[i].is_main) {
                main_category = this.list[i];
                break;
            }
        }
        return main_category;
    }

    computeJson() {
        const json_list = [];
        this.getList().forEach((category) => {
            json_list.push(category.computeJson());
        });
        return json_list;
    }

    computeUrlParameters() {
        return this.list.length > 0 ? { categories: this.list.map(i => i.name) } : undefined;
    }

    computeFilterFromUrl(filter) {
        if (!filter) {
            return;
        }
        this.list = (Array.isArray(filter) ? filter : [filter]).map(cat => new Category(cat));
    }

    // Private methods
    _setMainCategory(index) {
        for (let i = 0; i < this.getList().length; i++) {
            if (i == index) {
                this.getList()[i].setIsMain(true);
            }
            else {
                this.getList()[i].setIsMain(false);
            }
        }
    }

    _hasMainCategory() {
        for (let i = 0; i < this.getList().length; i++) {
            if (this.getList()[i].is_main) {
                return true;
            }
        }
        return false;
    }
}

export const category_manager = reactive(new CategoryManager());