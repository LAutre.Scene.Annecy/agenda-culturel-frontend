import { Manager } from "~/adapters/manager.js";

export class LinkManager extends Manager {
    constructor() {
        super();
    }

    getWebsiteUrl() {
        let website_url = "";
        this.getList().forEach(link => {
            if(link.isWebsiteLink()) {
                website_url = (link.value != null) ? link.value : "";
            }
        });
        return website_url;
    }

    computeSocialNetworksUrlsJson() {
        let json = "";
        this.getList().forEach(link => {
            if(!link.isWebsiteLink())
            json += link.toString();
        });
        return json;
    }
}