import { date_manager } from "./date_manager.js";
import { category_manager } from "./category_manager.js";
import { holder_manager } from "./holder_manager.js";
import { setOptionalField } from "./utils.js";

export function computeJson() {
    let event_data = {
        name: current_event.name,
        description: current_event.description
    };
    event_data = setOptionalField(event_data, "thumbnail", current_event.thumbnail);

    let other_data = {
        holders: holder_manager.computeHoldersJson(),
        categories: category_manager.computeJson(),
        organizers: holder_manager.computeOrganizersJson(),
        prices: date_manager.computePricesJson(),
        dates: date_manager.computeDatesJson()
    };

    let json_data = Object.assign({}, event_data, other_data);

    // console.log(json_data);
    return json_data;
}

export function resetEvent() {
    current_event.reset();
    holder_manager.reset();
    category_manager.reset();
    date_manager.reset();
}

class Event {
    constructor() {
        this.name = null;
        this.thumbnail = null;
        this.description = null;
        this.post_event_url = "https://www.example.com"; // TODO
    }

    reset() {
        this.name = null;
        this.thumbnail = null;
        this.description = null;
        this.post_event_url = "https://www.example.com"; // TODO
    }

    checkRequiredData() {
        if(this.name == null) {
            return false;
        }
        else if(this.description == null) {
            return false;
        }
        return true;
    }
}

export const current_event = reactive(new Event());