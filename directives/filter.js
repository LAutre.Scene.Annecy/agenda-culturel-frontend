
import { EventFilter } from "~/adapters/filter.js";

export const filter_date = ref();

export const filter = reactive(new EventFilter());
export const active_filter = reactive(new EventFilter());